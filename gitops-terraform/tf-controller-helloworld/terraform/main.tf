terraform {
  required_version = ">= 0.12.26"
}

variable "subject" {
   type = string
   default = "tfctl-rc"
   description = "Subject to hello"
}

resource "null_resource" "readcontentfile" {
  provisioner "local-exec" {
   command = "echo testing >> testing.txt"
  }
}

output "hello_world" {
  value = "hey hey ya, ${var.subject}! testing"
}
